package cz.zonky.vanek.task;

import cz.zonky.vanek.repository.LoanRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Tomas Vanek <tomas.vanek.26@gmail.com>
 * @since 2016-10-18
 */
public class LoanFetchingTaskTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private LoanRepository loanRepository;

    private LoanFetchingTask task;

    @Before
    public void setUp() throws Exception {
        task = new LoanFetchingTask(loanRepository);
    }

    @Test
    public void loadRecentLoans() throws Exception {
        LocalDateTime start = LocalDateTime.now();

        task.loadRecentLoans();

        ArgumentCaptor<LocalDateTime> rate1 = ArgumentCaptor.forClass(LocalDateTime.class);
        verify(loanRepository).findAllByCoveredFalseAndPublishDateNotAfter(rate1.capture());
        verifyNoMoreInteractions(loanRepository);
        assertFalse(start.isAfter(rate1.getValue()));

        task.loadRecentLoans();

        ArgumentCaptor<LocalDateTime> rate2 = ArgumentCaptor.forClass(LocalDateTime.class);
        verify(loanRepository).findAllByCoveredFalseAndPublishDateBetween(eq(rate1.getValue()), rate2.capture());
        verifyNoMoreInteractions(loanRepository);
        assertFalse(rate1.getValue().isAfter(rate2.getValue()));

        task.loadRecentLoans();

        ArgumentCaptor<LocalDateTime> rate3 = ArgumentCaptor.forClass(LocalDateTime.class);
        verify(loanRepository).findAllByCoveredFalseAndPublishDateBetween(eq(rate2.getValue()), rate3.capture());
        verifyNoMoreInteractions(loanRepository);
        assertFalse(rate2.getValue().isAfter(rate3.getValue()));
    }
}