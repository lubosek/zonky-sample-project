package cz.zonky.vanek;

import cz.zonky.vanek.model.Loan;
import cz.zonky.vanek.repository.LoanRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleProjectApplicationTests {

    @Autowired
    private LoanRepository loanRepository;

    @Test
    public void healthCheck() {
        List<Loan> loans = loanRepository.findAll();
        assertTrue("Zonky REST API is unavailable", !loans.isEmpty());
    }
}
