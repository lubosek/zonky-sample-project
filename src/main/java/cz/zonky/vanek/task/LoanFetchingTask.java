package cz.zonky.vanek.task;

import cz.zonky.vanek.model.Loan;
import cz.zonky.vanek.repository.LoanRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * A task that performs periodic fetching of the most recent loans from the market.
 *
 * @author Tomas Vanek <tomas.vanek.26@gmail.com>
 * @since 2016-10-17
 */
@Component
public class LoanFetchingTask {

    private static final Logger logger = LoggerFactory.getLogger(LoanFetchingTask.class);

    private final LoanRepository loanRepository;

    private LocalDateTime lastUpdate;

    @Autowired
    public LoanFetchingTask(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Scheduled(fixedRateString = "${loan.fetch.rate}")
    protected void loadRecentLoans() {
        LocalDateTime now = LocalDateTime.now();
        List<Loan> recentLoans;

        if (lastUpdate == null) {
            recentLoans = loanRepository.findAllByCoveredFalseAndPublishDateNotAfter(now);
        } else {
            recentLoans = loanRepository.findAllByCoveredFalseAndPublishDateBetween(lastUpdate, now);
        }
        lastUpdate = now;

        logger.info("The following loans have been fetched: {}", recentLoans);
    }
}
