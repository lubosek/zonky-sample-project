package cz.zonky.vanek.model;

import com.google.common.base.MoreObjects;

import java.math.BigDecimal;

/**
 * A model object that encapsulates data about a loan.
 *
 * @author Tomas Vanek <tomas.vanek.26@gmail.com>
 * @since 2016-10-17
 */
public class Loan {

    private Long id;
    private String name;
    private String story;
    private String purpose;
    private String nickName;
    private Long termInMonths;
    private BigDecimal interestRate;
    private String rating;
    private BigDecimal amount;
    private BigDecimal remainingInvestment;
    private BigDecimal investmentRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getTermInMonths() {
        return termInMonths;
    }

    public void setTermInMonths(Long termInMonths) {
        this.termInMonths = termInMonths;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getRemainingInvestment() {
        return remainingInvestment;
    }

    public void setRemainingInvestment(BigDecimal remainingInvestment) {
        this.remainingInvestment = remainingInvestment;
    }

    public BigDecimal getInvestmentRate() {
        return investmentRate;
    }

    public void setInvestmentRate(BigDecimal investmentRate) {
        this.investmentRate = investmentRate;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .toString();
    }
}
