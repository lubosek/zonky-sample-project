package cz.zonky.vanek.repository.impl;

import cz.zonky.vanek.model.Loan;
import cz.zonky.vanek.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The default implementation of the {@link LoanRepository} interface.
 *
 * @author Tomas Vanek <tomas.vanek.26@gmail.com>
 * @since 2016-10-17
 */
@Repository
public class LoanRepositoryImpl implements LoanRepository {

    private final RestTemplate restTemplate;
    private final String serviceUrl;

    @Autowired
    public LoanRepositoryImpl(RestTemplate restTemplate, @Value("${zonky.api.url}") String serviceUrl) {
        this.restTemplate = restTemplate;
        this.serviceUrl = serviceUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Loan findOne(long id) {
        return restTemplate.getForObject(serviceUrl + "/loans/{id}", Loan.class, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Loan> findAll() {
        Loan[] loans = restTemplate.getForObject(serviceUrl + "/loans/marketplace", Loan[].class);
        return Arrays.asList(loans);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Loan> findAllByCoveredFalseAndPublishDateNotAfter(LocalDateTime date) {
        checkNotNull(date, "date must not be null");

        Loan[] loans = restTemplate.getForObject(
                serviceUrl + "/loans/marketplace?covered__eq=false&datePublished__lte={date}",
                Loan[].class, date.toInstant(ZoneOffset.UTC));

        return Arrays.asList(loans);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Loan> findAllByCoveredFalseAndPublishDateBetween(LocalDateTime from, LocalDateTime to) {
        checkNotNull(from, "from date must not be null");
        checkNotNull(to, "to date must not be null");

        Loan[] loans = restTemplate.getForObject(
                serviceUrl + "/loans/marketplace?covered__eq=false&datePublished__gt={from}&datePublished__lte={to}",
                Loan[].class, from.toInstant(ZoneOffset.UTC), to.toInstant(ZoneOffset.UTC));

        return Arrays.asList(loans);
    }
}
