package cz.zonky.vanek.repository;

import cz.zonky.vanek.model.Loan;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Interface providing basic operations for loan collection.
 *
 * @author Tomas Vanek <tomas.vanek.26@gmail.com>
 * @since 2016-10-17
 */
public interface LoanRepository {

    /**
     * Returns an instance of {@link Loan} with a specified id, or null if no result is found.
     *
     * @return an instance of {@link Loan}, or null value
     */
    Loan findOne(long id);

    /**
     * Returns a list of all {@link Loan} instances.
     *
     * @return a list of {@link Loan} instances, or an empty list
     */
    List<Loan> findAll();

    /**
     * Returns a list of loans whose publish date is not after a specified date, and which are still waiting for investors.
     *
     * @param date max date
     * @return a list of {@link Loan} instances, or an empty list
     */
    List<Loan> findAllByCoveredFalseAndPublishDateNotAfter(LocalDateTime date);

    /**
     * Returns a list of loans whose publish date is in a specified date range, and which are still waiting for investors.
     *
     * @param from start date
     * @param to   end date
     * @return a list of {@link Loan} instances, or an empty list
     */
    List<Loan> findAllByCoveredFalseAndPublishDateBetween(LocalDateTime from, LocalDateTime to);

}
